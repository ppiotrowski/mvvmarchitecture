package piotrowski.mvvmcompletearchitecture;

import android.os.Bundle;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.subjects.BehaviorSubject;


@Singleton
public class CommunicationBus {

    private final BehaviorSubject<Bundle> bus = BehaviorSubject.create();

    @Inject
    public CommunicationBus() {

    }

    public void send(Bundle bundle) {
        bus.onNext(bundle);
    }

    public Observable<Bundle> toObservable() {
        return bus;
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }
}
