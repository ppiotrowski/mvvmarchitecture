package piotrowski.mvvmcompletearchitecture;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;

import piotrowski.mvvmcompletearchitecture.dagger.component.ApplicationComponent;
import piotrowski.mvvmcompletearchitecture.dagger.component.BaseComponent;
import piotrowski.mvvmcompletearchitecture.navigation.Navigator;
import timber.log.Timber;

public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private static final int COMPONENT_CACHE_CAPACITY = 5;
    private Navigator navigator;
    private ApplicationComponent applicationComponent;
    private ArrayMap<String, BaseComponent> componentCache;

    @Override
    public void onCreate() {
        super.onCreate();
        this.componentCache = new ArrayMap<>(COMPONENT_CACHE_CAPACITY);
        this.initializeInjector();
        initNavigator();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
//            if (LeakCanary.isInAnalyzerProcess(this)) {
//                // This process is dedicated to LeakCanary for heap analysis.
//                // You should not init your app in this process.
//                return;
//            }
//            LeakCanary.install(this);
        }
    }

    private void initNavigator() {
        navigator = applicationComponent.navigator();
        registerActivityLifecycleCallbacks(this);
    }

    public void initializeInjector() {
        this.applicationComponent = ApplicationComponent.Initializer.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null)
            initializeInjector();
        return this.applicationComponent;
    }

    public void cacheComponent(String key, BaseComponent component) {
        componentCache.put(key, component);
    }

    public BaseComponent getCachedComponent(String key) {
        return componentCache.get(key);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        navigator.setCurrentActivity(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Activity navigatorActivity = navigator.getCurrentActivity();
        if (navigatorActivity == null || navigatorActivity != activity)
            navigator.setCurrentActivity(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        navigator.destroy(activity);
    }
}
