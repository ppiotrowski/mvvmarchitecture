package piotrowski.mvvmcompletearchitecture.adapter;

import android.view.View;

public interface ClickHandler<T> {
    void onClick(View view, T item);
}