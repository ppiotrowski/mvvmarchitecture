package piotrowski.mvvmcompletearchitecture.dagger.component;


import dagger.Subcomponent;
import piotrowski.data.dagger.ActivityScope;
import piotrowski.mvvmcompletearchitecture.dagger.module.ActivityModule;


/**
 * Created by papiot on 2015-10-05.
 */
@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent extends BaseComponent {

    String KEY = "activity_component";

    class Initializer {
        private Initializer() {
//            no instance
        }

        public static ActivityComponent init(ApplicationComponent applicationComponent) {
            return applicationComponent
                    .activitySubcomponent(new ActivityModule());
        }
    }
}
