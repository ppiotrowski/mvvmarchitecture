package piotrowski.mvvmcompletearchitecture.dagger.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import piotrowski.data.dagger.NetworkModule;
import piotrowski.mvvmcompletearchitecture.dagger.module.ActivityModule;
import piotrowski.mvvmcompletearchitecture.dagger.module.ApplicationModule;
import piotrowski.mvvmcompletearchitecture.navigation.Navigator;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent extends BaseComponent {

    Navigator navigator();

    ActivityComponent activitySubcomponent(ActivityModule activityModule);

    class Initializer {
        private Initializer() {
//            no instance
        }

        public static ApplicationComponent init(Context context) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(context))
                    .networkModule(new NetworkModule())
                    .build();
        }
    }
}
