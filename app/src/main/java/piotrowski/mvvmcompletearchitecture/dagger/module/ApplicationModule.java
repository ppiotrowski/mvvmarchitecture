package piotrowski.mvvmcompletearchitecture.dagger.module;

import android.content.Context;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import piotrowski.mvvmcompletearchitecture.NavigationBus;
import piotrowski.mvvmcompletearchitecture.navigation.Navigator;
import piotrowski.mvvmcompletearchitecture.navigation.handler.ActivityNavigationHandler;
import piotrowski.mvvmcompletearchitecture.navigation.handler.DialogNavigationHandler;
import piotrowski.mvvmcompletearchitecture.navigation.handler.FragmentNavigationHandler;
import piotrowski.mvvmcompletearchitecture.navigation.handler.NavigationHandler;
import piotrowski.mvvmcompletearchitecture.navigation.handler.PopActivityStackHandler;
import piotrowski.mvvmcompletearchitecture.navigation.handler.PopFragmentStackHandler;

/**
 * Created by papiot on 2015-10-05.
 */

@Singleton
@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new Gson();
    }

    @Singleton
    @Provides
    Navigator provideNavigator(NavigationBus navigationBus, List<NavigationHandler> navigationHandlers) {
        return new Navigator(navigationBus, navigationHandlers);
    }

    @Singleton
    @Provides
    List<NavigationHandler> provideNavigationHandlers() {
        return Arrays.asList(
                new ActivityNavigationHandler(),
                new FragmentNavigationHandler(),
                new DialogNavigationHandler(),
                new PopFragmentStackHandler(),
                new PopActivityStackHandler()
        );
    }

    @Singleton
    @Provides
    Realm provideRealm(RealmConfiguration configuration) {
        return Realm.getInstance(configuration);
    }

    @Singleton
    @Provides
    RealmConfiguration provideRealmConfiguration() {
        return new RealmConfiguration.Builder(context)
                .schemaVersion(1)
                .build();
    }
}
