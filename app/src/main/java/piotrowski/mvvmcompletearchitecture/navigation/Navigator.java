package piotrowski.mvvmcompletearchitecture.navigation;

import android.app.Activity;

import java.util.List;

import piotrowski.mvvmcompletearchitecture.NavigationBus;
import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;
import piotrowski.mvvmcompletearchitecture.navigation.handler.NavigationHandler;


/**
 * Created by papiot on 2016-03-01.
 */

public class Navigator {

    private final NavigationBus navigationBus;
    private final List<NavigationHandler> navigationHandlers;
    private Activity currentActivity;

    public Navigator(NavigationBus navigationBus, List<NavigationHandler> navigationHandlers) {
        this.navigationBus = navigationBus;
        this.navigationHandlers = navigationHandlers;
        listen();
    }

    private void listen() {
        navigationBus.toObservable().subscribe(this::handleEvent);
    }

    private void handleEvent(NavigationEvent event) {
        for (NavigationHandler navigationHandler : navigationHandlers) {
            if (navigationHandler.canHandle(currentActivity, event)) {
                navigationHandler.handle(currentActivity, event);
                break;
            }
        }
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public void destroy(Activity activity) {
        if (currentActivity == activity)
            currentActivity = null;
    }
}
