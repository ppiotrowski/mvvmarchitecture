package piotrowski.mvvmcompletearchitecture.navigation.event;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.view.View;

/**
 * Created by papiot on 8/9/2016.
 */
public class ActivityNavigationEvent implements NavigationEvent {

    private boolean finishCaller;
    private Class activityToStart;
    private Bundle bundle;
    private Pair<View, String>[] sharedElements;

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
    }

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart, Bundle bundle) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
        this.bundle = bundle;
    }

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart, Bundle bundle, Pair<View, String>... sharedElements) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
        this.bundle = bundle;
        this.sharedElements = sharedElements;
    }

    public Class getActivityToStart() {
        return activityToStart;
    }

    public boolean shouldFinishCaller() {
        return finishCaller;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public Pair<View, String>[] getSharedElements() {
        return sharedElements;
    }
}
