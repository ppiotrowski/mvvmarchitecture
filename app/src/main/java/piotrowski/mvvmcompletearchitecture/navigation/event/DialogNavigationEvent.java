package piotrowski.mvvmcompletearchitecture.navigation.event;


import android.support.v4.app.DialogFragment;

/**
 * Created by papiot on 8/29/2016.
 */
public class DialogNavigationEvent implements NavigationEvent {

    private DialogFragment dialog;
    private String tag;

    public DialogNavigationEvent(DialogFragment dialog, String tag) {
        this.dialog = dialog;
        this.tag = tag;
    }

    public DialogFragment getDialog() {
        return dialog;
    }

    public String getTag() {
        return tag;
    }
}
