package piotrowski.mvvmcompletearchitecture.navigation.event;

/**
 * Created by papiot on 8/10/2016.
 */
public class DrawerNavigationEvent implements NavigationEvent {

    private CharSequence itemName;
    private int itemId;

    public DrawerNavigationEvent(int itemId) {
        this.itemId = itemId;
    }

    public DrawerNavigationEvent(CharSequence itemName, int itemId) {
        this.itemName = itemName;
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }

    public CharSequence getItemName() {
        return itemName;
    }
}
