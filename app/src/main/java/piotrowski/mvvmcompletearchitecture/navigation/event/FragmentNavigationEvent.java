package piotrowski.mvvmcompletearchitecture.navigation.event;

import android.support.v4.app.Fragment;

/**
 * Created by papiot on 8/31/2016.
 */
public class FragmentNavigationEvent implements NavigationEvent {

    private Fragment fragment;
    private String tag;
    private boolean addToBackStack;

    public FragmentNavigationEvent(Fragment fragment, String tag, boolean addToBackStack) {
        this.fragment = fragment;
        this.tag = tag;
        this.addToBackStack = addToBackStack;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public String getTag() {
        return tag;
    }

    public boolean addToBackStack() {
        return addToBackStack;
    }
}
