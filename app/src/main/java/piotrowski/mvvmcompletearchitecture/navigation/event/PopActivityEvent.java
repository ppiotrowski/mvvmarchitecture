package piotrowski.mvvmcompletearchitecture.navigation.event;

/**
 * Created by papiot on 9/27/2016.
 */

public class PopActivityEvent implements NavigationEvent {

    private boolean clearStack;

    public PopActivityEvent(boolean clearStack) {
        this.clearStack = clearStack;
    }

    public boolean isClearStack() {
        return clearStack;
    }
}
