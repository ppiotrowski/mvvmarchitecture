package piotrowski.mvvmcompletearchitecture.navigation.handler;

import android.app.Activity;

import piotrowski.mvvmcompletearchitecture.navigation.event.DialogNavigationEvent;
import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;
import piotrowski.mvvmcompletearchitecture.view.BaseActivity;

/**
 * Created by papiot on 8/29/2016.
 */
public class DialogNavigationHandler implements NavigationHandler<BaseActivity,DialogNavigationEvent> {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof DialogNavigationEvent;
    }

    @Override
    public void handle(BaseActivity activity, DialogNavigationEvent event) {
        event.getDialog().show(activity.getSupportFragmentManager(), event.getTag());
    }
}
