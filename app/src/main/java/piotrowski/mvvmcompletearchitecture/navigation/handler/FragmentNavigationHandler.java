package piotrowski.mvvmcompletearchitecture.navigation.handler;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import piotrowski.mvvmcompletearchitecture.R;
import piotrowski.mvvmcompletearchitecture.navigation.event.FragmentNavigationEvent;
import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;
import piotrowski.mvvmcompletearchitecture.view.BaseActivity;

/**
 * Created by papiot on 9/1/2016.
 */
public class FragmentNavigationHandler implements NavigationHandler<BaseActivity, FragmentNavigationEvent> {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof FragmentNavigationEvent;
    }

    @Override
    public void handle(BaseActivity activity, FragmentNavigationEvent event) {
        FragmentManager fm = activity.getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(event.getTag());
        if (fragment == null)
            fragment = event.getFragment();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, event.getTag());
        if (event.addToBackStack())
            transaction.addToBackStack(event.getTag());
        transaction.commit();
    }
}
