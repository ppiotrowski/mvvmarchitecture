package piotrowski.mvvmcompletearchitecture.navigation.handler;

import android.app.Activity;

import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;

public interface NavigationHandler<A extends Activity, T extends NavigationEvent> {

    boolean canHandle(Activity activity, NavigationEvent navigationEvent);

    void handle(A activity, T event);
}
