package piotrowski.mvvmcompletearchitecture.navigation.handler;

import android.app.Activity;
import android.content.Intent;

import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;
import piotrowski.mvvmcompletearchitecture.navigation.event.PopActivityEvent;
import piotrowski.mvvmcompletearchitecture.view.MainActivity;

public class PopActivityStackHandler implements NavigationHandler {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof PopActivityEvent;
    }

    @Override
    public void handle(Activity activity, NavigationEvent event) {
        if (((PopActivityEvent) event).isClearStack()) {
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
        } else {
            if (activity != null)
                activity.finish();
        }
    }
}
