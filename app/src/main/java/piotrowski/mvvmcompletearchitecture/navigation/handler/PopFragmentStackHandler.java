package piotrowski.mvvmcompletearchitecture.navigation.handler;

import android.app.Activity;

import piotrowski.mvvmcompletearchitecture.navigation.event.NavigationEvent;
import piotrowski.mvvmcompletearchitecture.navigation.event.PopFragmentEvent;
import piotrowski.mvvmcompletearchitecture.view.BaseActivity;

public class PopFragmentStackHandler implements NavigationHandler<BaseActivity, PopFragmentEvent> {
    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof PopFragmentEvent;
    }

    @Override
    public void handle(BaseActivity activity, PopFragmentEvent event) {
        activity.getSupportFragmentManager().popBackStack();
    }
}
