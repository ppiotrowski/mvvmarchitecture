package piotrowski.mvvmcompletearchitecture.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import piotrowski.data.manager.CredentialsManager;

/**
 * Created by papiot on 8/11/2016.
 */
public class BindingUtils {

    @BindingAdapter(value = {"image"})
    public static void setImage(ImageView imageView, int imageId) {
        CredentialsManager credentialsManager = new CredentialsManager(imageView.getContext());
        String url = "http://"; // TODO: 11/30/2016  add image url
        Picasso picasso;
        picasso = new Picasso.Builder(imageView.getContext()).listener((picasso1, uri, exception) -> {
            exception.printStackTrace();
        }).build();
        RequestCreator requestCreator = picasso.load(url);
        requestCreator.fit().centerCrop();
        requestCreator.into(imageView);
    }
}
