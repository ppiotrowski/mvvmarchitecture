package piotrowski.mvvmcompletearchitecture.utils;

public interface OnRevealAnimationListener {
	void onRevealFinish();
}
