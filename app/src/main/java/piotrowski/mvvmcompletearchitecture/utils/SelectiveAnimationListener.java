package piotrowski.mvvmcompletearchitecture.utils;

import android.view.animation.Animation;

/**
 * Created by Paveu on 2016-03-03.
 */
public abstract class SelectiveAnimationListener implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
