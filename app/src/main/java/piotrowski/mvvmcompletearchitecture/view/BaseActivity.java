package piotrowski.mvvmcompletearchitecture.view;

import android.databinding.Observable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

import piotrowski.mvvmcompletearchitecture.MyApplication;
import piotrowski.mvvmcompletearchitecture.dagger.component.ActivityComponent;
import piotrowski.mvvmcompletearchitecture.dagger.component.ApplicationComponent;


/**
 * Created by papiot on 2015-10-05.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    protected void injectMembers() {
    }

    protected void initializeOnViewModelPropertyChangeListener() {
    }

    protected void addOnPropertyChangeListener() {
    }

    protected void removeOnPropertyChangeListener() {
    }

    protected void onPropertyChange(Observable sender, int propertyId) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            checkComponentCache();
        initializeInjector();
        injectMembers();
        initializeOnViewModelPropertyChangeListener();
        addOnPropertyChangeListener();
    }

    private void checkComponentCache() {
        activityComponent = (ActivityComponent) ((MyApplication)getApplication()).getCachedComponent(ActivityComponent.KEY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeOnPropertyChangeListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activityComponent == null) {
            initializeInjector();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ((MyApplication) getApplication()).cacheComponent(ActivityComponent.KEY, activityComponent);
    }

    public void initializeInjector() {
        if (activityComponent == null)
            activityComponent = ActivityComponent.Initializer.init(getApplicationComponent());
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null)
            initializeInjector();
        return activityComponent;

    }

    public ApplicationComponent getApplicationComponent() {
        return ((MyApplication) getApplication()).getApplicationComponent();
    }

    protected void setTransitions() {

    }

    protected static class OnViewModelPropertyChange<T extends BaseActivity> extends Observable.OnPropertyChangedCallback {

        private final WeakReference<T> weakReference;

        public OnViewModelPropertyChange(T reference) {
            weakReference = new WeakReference<>(reference);
        }

        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            T reference = weakReference.get();
            if (reference != null)
                reference.onPropertyChange(sender, propertyId);
        }
    }
}
