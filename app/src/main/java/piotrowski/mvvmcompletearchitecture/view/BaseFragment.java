package piotrowski.mvvmcompletearchitecture.view;

import android.databinding.Observable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.lang.ref.WeakReference;

import piotrowski.mvvmcompletearchitecture.dagger.component.ActivityComponent;
import piotrowski.mvvmcompletearchitecture.dagger.component.ApplicationComponent;

/**
 * Created by papiot on 8/8/2016.
 */
public abstract class BaseFragment extends Fragment {

    protected abstract void injectMembers();

    protected abstract ViewDataBinding getBinding();

    protected void initializeOnViewModelPropertyChangeListener() {
    }

    protected void addOnPropertyChangeListener() {
    }

    protected void removeOnPropertyChangeListener() {}

    protected void onPropertyChange(Observable sender, int propertyId) {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();
        initializeOnViewModelPropertyChangeListener();
        addOnPropertyChangeListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeOnPropertyChangeListener();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getBinding() != null)
            getBinding().unbind();
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public ActivityComponent getActivityComponent() {
        try {
            return ((BaseActivity) getActivity()).getActivityComponent();
        } catch (ClassCastException e) {
            throw new RuntimeException("Every Activity should extend BaseActivity!");
        }
    }

    public ApplicationComponent getApplicationComponent() {
        try {
            return ((BaseActivity) getActivity()).getApplicationComponent();
        } catch (ClassCastException e) {
            throw new RuntimeException("Every Activity should extend BaseActivity!");
        }
    }

    protected void setTransitions() {

    }

    protected static class OnViewModelPropertyChange<T extends BaseFragment> extends Observable.OnPropertyChangedCallback {

        private final WeakReference<T> weakReference;

        public OnViewModelPropertyChange(T reference) {
            weakReference = new WeakReference<>(reference);
        }

        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            T reference = weakReference.get();
            if (reference != null)
                reference.onPropertyChange(sender, propertyId);
        }
    }
}
