package piotrowski.mvvmcompletearchitecture.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import piotrowski.mvvmcompletearchitecture.R;
import piotrowski.mvvmcompletearchitecture.databinding.MainActivityBinding;

public class MainActivity extends BaseActivity {

    private MainActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
    }
}