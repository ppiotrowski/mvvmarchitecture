package piotrowski.mvvmcompletearchitecture.viewmodel;


import android.databinding.BaseObservable;

import piotrowski.mvvmcompletearchitecture.NavigationBus;

public abstract class BaseViewModel extends BaseObservable {

    protected NavigationBus navigationBus;

    public BaseViewModel(NavigationBus navigationBus) {
        this.navigationBus = navigationBus;
    }
}
