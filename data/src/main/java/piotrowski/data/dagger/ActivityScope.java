package piotrowski.data.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by papiot on 2015-10-05.
 */
@Retention(RetentionPolicy.RUNTIME)
@Scope
public @interface ActivityScope {
}
