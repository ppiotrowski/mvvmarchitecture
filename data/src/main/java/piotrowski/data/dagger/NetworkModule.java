package piotrowski.data.dagger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import piotrowski.data.manager.CredentialsManager;
import piotrowski.data.network.HeadersInterceptor;
import piotrowski.data.network.LoggingInterceptor;
import piotrowski.data.utils.EnumRetrofitConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl("http://") // TODO: 2016-01-20
                .client(okHttpClient)
                .addConverterFactory(new EnumRetrofitConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(List<Interceptor> interceptorList) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(3, TimeUnit.MINUTES);
        builder.readTimeout(3, TimeUnit.MINUTES);
        builder.writeTimeout(3, TimeUnit.MINUTES);
        for (Interceptor i : interceptorList) {
            builder.addInterceptor(i);
        }

        return builder.build();
    }

    @Provides
    @Singleton
    List<Interceptor> provideInterceptors(CredentialsManager credentialsManager) {
        return Arrays.asList(new HeadersInterceptor(credentialsManager), new LoggingInterceptor());
    }
}
