package piotrowski.data.interactor;


import rx.Observable;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by paveu on 2016-02-25.
 */
public abstract class BaseInteractor {

    private Subscription subscription = Subscriptions.empty();

    protected abstract Observable buildInteractorObservable(ExtendedSubscriber extendedSubscriber);

    @SuppressWarnings("unchecked")
    public void execute(ExtendedSubscriber extendedSubscriber) {
        subscription = buildInteractorObservable(extendedSubscriber).subscribe(extendedSubscriber);
    }

    protected <T> Observable.Transformer<T, T> applyExtendedActions(ExtendedSubscriber extendedSubscriber) {
        return observable -> observable
                .doOnSubscribe(extendedSubscriber::onSubscribe)
                .doOnTerminate(extendedSubscriber::onTerminate);
    }

    public void unsubscribe() {
        if (!subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
