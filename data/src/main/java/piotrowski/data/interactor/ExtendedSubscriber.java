package piotrowski.data.interactor;

import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * Created by Paveu on 2016-02-28.
 */
public abstract class ExtendedSubscriber<T> extends Subscriber<T> {

    public abstract void onSubscribe();

    public abstract void onTerminate();

    public void onServerError(int error) {
    }

    public abstract void onServerError(String error);

    @Override
    public void onError(Throwable e) {

        if (e instanceof HttpException && ((HttpException) e).code() != 401)
            parseError((HttpException) e);
        else if (e instanceof UnknownHostException)
            parseError((UnknownHostException) e);
        else if (e instanceof HttpException && ((HttpException) e).code() == 500) {
//            onServerError(); // TODO: 11/29/2016 server error
        } else {
//            onServerError(); // TODO: 11/29/2016 unknown error
        }
    }

    private void parseError(UnknownHostException e) {
        onServerError(e.getMessage());
    }

    private void parseError(HttpException e) {
        // TODO: 11/29/2016 parse errors
    }
}
