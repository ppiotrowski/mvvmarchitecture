package piotrowski.data.manager;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import piotrowski.data.utils.Preferences;


/**
 * Created by papiot on 2015-10-07.
 */

@Singleton
public class CredentialsManager extends Preferences {

    public static final String AUTH_TOKEN = "auth_token";
    public static final String USER_ID = "user_id";

    @Inject
    public CredentialsManager(Context context) {
        super(context);
    }

    public String getAuthToken() {
        return sharedPreferences.getString(AUTH_TOKEN, "");
    }

    public void setAuthToken(String token) {
        sharedPreferences.edit().putString(AUTH_TOKEN, token).apply();
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "");
    }

    public void setUserId(String userId) {
        sharedPreferences.edit().putString(USER_ID, userId).apply();
    }
}
