package piotrowski.data.network;


import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import piotrowski.data.manager.CredentialsManager;

@Singleton
public class HeadersInterceptor implements Interceptor {

    private CredentialsManager credentialsManager;

    @Inject
    public HeadersInterceptor(CredentialsManager credentialsManager) {
        this.credentialsManager = credentialsManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        String token = credentialsManager.getAuthToken();
        String userId = credentialsManager.getUserId();

        Request.Builder requestBuilder = originalRequest.newBuilder()
                .url(originalRequest.url())
                .method(originalRequest.method(), originalRequest.body());

        if (!token.isEmpty() && !userId.isEmpty()) {
            // TODO: 11/30/2016 add headers
        }

        return chain.proceed(requestBuilder.build());
    }
}
