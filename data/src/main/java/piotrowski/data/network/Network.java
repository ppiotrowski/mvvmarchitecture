package piotrowski.data.network;

import javax.inject.Inject;
import javax.inject.Singleton;

import piotrowski.data.manager.CredentialsManager;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;


/**
 * Created by papiot on 2015-10-05.
 */
@Singleton
public class Network {

    private Retrofit retrofit;
    private CredentialsManager credentialsManager;
    private Api api;

    @Inject
    public Network(Retrofit retrofit, CredentialsManager credentialsManager) {
        this.retrofit = retrofit;
        this.credentialsManager = credentialsManager;
        buildApi();
    }

    private void buildApi() {
        api = retrofit.create(Api.class);
    }

    public Api getApi() {
        return api;
    }

    public <T> Observable<T> execute(Observable<T> request) {
        return addUnauthorizedRetryStrategy(request);
    }

    private <T> Observable<T> addUnauthorizedRetryStrategy(Observable<T> toBeResumed) {
        return toBeResumed.onErrorResumeNext(throwable -> {
            if (isUnauthorizedError(throwable)) {
                return buildAuthorizationObservable(toBeResumed);
            }
            return Observable.error(throwable);
        });
    }

    private boolean isUnauthorizedError(Throwable throwable) {
        return (throwable instanceof HttpException && ((HttpException) throwable).code() == 401);
    }

    private <T> Observable<T> buildAuthorizationObservable(Observable<T> toBeResumed) {
        return null; // TODO: 11/29/2016  
    }
}
