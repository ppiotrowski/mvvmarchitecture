package piotrowski.data.repository;

import java.util.Collection;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import piotrowski.data.network.Network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Paveu on 2016-02-27.
 */
public abstract class BaseRepository {

    protected Network network;
    protected Realm realm;

    public BaseRepository(Network network, Realm realm) {
        this.network = network;
        this.realm = realm;
    }

    protected  <T extends RealmObject> void addToRealm(Collection<T> collection) {
        realm.executeTransactionAsync(realm1 -> realm1.copyToRealmOrUpdate(collection));
    }

    protected void deleteFromRealm(Class<? extends RealmModel> clazz) {
        realm.executeTransactionAsync(realm1 -> realm1.delete(clazz));
    }

    protected <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
