package piotrowski.data.utils;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by papiot on 9/13/2016.
 */
public class EnumRetrofitConverterFactory extends Converter.Factory {
    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Converter<?, String> converter = null;
        if (type instanceof Class && ((Class<?>) type).isEnum()) {
            converter = value -> EnumUtils.getSerializedNameValue((Enum) value);
        }
        return converter;
    }

    public static class EnumUtils {

        static public <E extends Enum<E>> String getSerializedNameValue(E e) {
            String value = null;
            try {
                value = e.getClass().getField(e.name()).getAnnotation(SerializedName.class).value();
            } catch (NoSuchFieldException exception) {
                value = e.name();
            }
            return value.toLowerCase();
        }
    }
}
