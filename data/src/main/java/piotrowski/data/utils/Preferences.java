package piotrowski.data.utils;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by papiot on 2015-10-05.
 */

@Singleton
public class Preferences {

    public static final String PREFS = "prefs";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String GCM_TOKEN = "gcmToken";

    protected SharedPreferences sharedPreferences;


    @Inject
    public Preferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public boolean isDeviceTokenSent() {
        return sharedPreferences.getBoolean(SENT_TOKEN_TO_SERVER, false);
    }

    public void setDeviceTokenSent(boolean sent) {
        sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, sent).apply();
    }

    public void saveToken(String token) {
        sharedPreferences.edit().putString(GCM_TOKEN, token).apply();
    }

    public String getToken() {
        return sharedPreferences.getString(GCM_TOKEN, "");
    }

    public void clearPrefs() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
